-- liquibase formatted sql

-- changeset jorgeflopezm:7
CREATE TABLE "portal"."test2" (
 id INT PRIMARY KEY,
 product_name VARCHAR(50) NOT NULL
);

--rollback DROP TABLE "portal"."test2";