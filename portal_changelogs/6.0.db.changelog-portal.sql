-- liquibase formatted sql

-- changeset jorgeflopezm:8
CREATE TABLE "portal"."test3" (
    id INT PRIMARY KEY,
    product_name VARCHAR(50) NOT NULL
);

--rollback drop table "portal"."test3";