-- liquibase formatted sql

-- changeset jorgeflopezm:6
CREATE TABLE "portal"."test1" (
    id CHARACTER(3) NOT NULL,
    name TEXT NOT NULL
);

--rollback DROP TABLE "portal"."test1";