-- liquibase formatted sql

-- changeset jorgeflopezm:10
CREATE TABLE test5 (
    id INTEGER NOT NULL, 
    name TEXT NOT NULL, 
    countrycode CHAR(3) NOT NULL, 
    district TEXT NOT NULL, 
    population INTEGER NOT NULL
);

--rollback DROP TABLE IF EXISTS test4;