-- liquibase formatted sql

-- changeset jorgeflopezm:3
CREATE TABLE demo2 (  
    id INT PRIMARY KEY,
    name TEXT NOT NULL
);

--rollback drop table demo2;