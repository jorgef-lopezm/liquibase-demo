-- liquibase formatted sql

-- changeset jorgeflopezm:4
CREATE TABLE demo3 (  
    id INT PRIMARY KEY,
    name TEXT NOT NULL
);

--rollback drop table demo3;

-- changeset jorgeflopezm:5
--precondition-sql-check expectedResult:0 SELECT COUNT(*) FROM demo3
insert into demo3 (id, name) values (1, 'name 1');

-- rollback DELETE FROM demo3 WHERE ID = 1;
