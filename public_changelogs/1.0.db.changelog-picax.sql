-- liquibase formatted sql

-- changeset jorgeflopezm:1
CREATE TABLE demo1 (  
    id INT PRIMARY KEY,
    name TEXT NOT NULL
);

--rollback drop table demo1; 

-- changeset jorgeflopezm:2
INSERT INTO demo1 (id, name) VALUES (1, 'name 1');
INSERT INTO demo1 (id, name) VALUES (2, 'name 2');

-- rollback DELETE FROM demo1 WHERE ID = 1; rollback DELETE FROM demo1 WHERE ID = 2;